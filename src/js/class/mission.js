export default class Mission{
    /**
     * Lister les mission
     * mettre les parametres en data
     * mettre un ecouteur qui disable le champ une fois rempli (avec un petit bouton modifier)
     */
    constructor(datas,id){
        this.name = datas.name;
        this.id = id;
        this.butins = datas.butins;
        this.victimes = datas.victimes;
        this.points = datas.points;
    }
    TemplateConstructor(){
        return '<div class="ligneMission grid-4">\n' +
            '          <div class="lieux w100">' + this.name + '</div>\n' +
            '          <div class="w100"><input class="" name="butin_'+this.name + '" id="butin_'+this.name + '" type="text"></div>\n' +
            '          <div class="w100"><input class="" name="victimes_'+this.name + '" id="victimes_'+this.name + '" type="text"></div>\n' +
            '          <div class="w100"><input class="" name="points_'+this.name + '" id="points_'+this.name + '" placeholder="/' + this.points + '" type="text"></div>\n' +
            '        </div>';
    }
    EcouteMissions(){
        this.inputs = {
            butin : document.getElementById('butin_'+this.name),
            victimes : document.getElementById('victimes_'+this.name),
            points : document.getElementById('points_'+this.name),
        }
    }
    CreateHtml()
    {
        let HTML = '<div class="ligneMission grid-4">' +
        '<div class="lieux w100">'+this.name+'</div>' +
        '<input type="hidden" name="id" value="'+this.id+'">' +
        '<div class="w100"><input class="" type="text" name="butin_'+this.id+'" id="butin_'+this.id+'"></div>' +
        '<div class="w100"><input class="" type="text" name="victimes_'+this.id+'" id="victimes_'+this.id+'"></div>' +
        '<div class="w100"><input class="" type="text" name="points_'+this.id+'" id="points_'+this.id+'" placeholder="/'+this.points+'"></div>' +
        '</div>';

        return HTML;
    }
}
export default class Player{
    constructor(){
        this.points = 0;

        this.levelPossible = [0,1,2];
        this.level = undefined
    }
    setLevel(level){
        if (this.level == undefined){
            this.level = level
            // Mise en place des points de compétences
            this.setPointsCompetences()
        }
    }
    setPointsCompetences(){
        if (this.level != undefined){
            switch (this.level){
                case 0 :
                    this.pointsCompetences = 99999999;
                    break;
                case 1 :
                    this.pointsCompetences = 5;
                    break;
                case 2 :
                    this.pointsCompetences = 2;
                    break;
            }
        }else{
            this.pointsCompetences = 0;
        }
    }
}
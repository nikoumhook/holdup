import Player from './player';
import Mission from './mission';
import Competence from './competence';


class Partie{
    constructor(datas){
        this.nbMissions = 0;
        this.player = new Player;
        this.Html = {};
        this.Html.missions = this.ChargeMissions(datas.missions);
        this.Html.competences = this.ChargeCompetences(datas.competences);
        this.Html.clinOeil = this.CreateClinOeil(datas.clinsOeil);
    }
    init(){
        this.journee = 0;
        // this.getValeurs();
        this.loadHtml()
        this.EcouteChoixNiveau() ;
        // this.ChargeCompetences();
        this.EcouteBoutonJournee();
        this.EcouteMissions();
    }
    ChargeCompetences(competences){
        var allCompetences = {};
        var HtmlComp = '';
        for (var i = 0; i < competences.length ; i++){
            allCompetences[competences[i].id] = new Competence(competences[i]);
            HtmlComp += allCompetences[competences[i].id].CreateHtml();
        }
        this.competences = allCompetences;

        return HtmlComp;
    }
    ChargeMissions(missions){
        var allMissions = {};
        var HtmlMission = '';
        var i=0;
        missions.forEach(function(mission){
            var tmp = new Mission(mission,i);
            allMissions[i++] = tmp;
            HtmlMission += tmp.CreateHtml();
        })
        this.nbMissions = i;
        this.missions = allMissions;
        return HtmlMission;
    }
    setJournee(journee){
       if (this.journee < 7 && journee <= 7 && journee >= 0){
           this.journee = journee;
       }
    }
    EcouteChoixNiveau(){
        this.player.levelPossible.forEach((e) => {
            document.getElementById('diffLvl_'+e).addEventListener("click", ()=>{
                this.player.setLevel(e);
                this.getValeurs();
            }, true);
        })
    }
    EcouteBoutonJournee(){
        for (let i = 1; i <= 7; i++){
            document.getElementById('journee_'+i).addEventListener("click", ()=>{
                this.setJournee(i);
            },true)
        }
    }
    EcouteMissions(){
        // TODO ce ne doit pas etre le click mais keypress ou autre + faire un methode pour chaque ajout dans mission
        for (let i = 0; i < this.nbMissions; i++){
            let butinEl = document.getElementById('butin_'+ i);
            let victimesEl = document.getElementById('victimes_'+ i);
            let pointsEl = document.getElementById('points_'+ i);
            butinEl.addEventListener("click", ()=>{
                console.log(this.missions[i]);
                console.log(butinEl.value);
            },true)
            victimesEl.addEventListener("click", ()=>{
                console.log('hello')
            },true)
            pointsEl.addEventListener("click", ()=>{
                console.log('hello')
            },true)
        }


    }
    CreateClinOeil(clinsOeil){
        var HTML = '';

        for (var i=0 ; i < clinsOeil.length; i++){
            var id = clinsOeil[i].name.replace('\'','').replace(' ','').substr(0,3).toLowerCase();

            if (i == 0){
                HTML += '<div class="col1">'
            }else if(i == '11'){
                HTML += '</div><div class="col2">'
            }

            var s = (clinsOeil[i].points > 1)? 's' : '';

            HTML += '<div class="clinOeil">'+
                '<label for="check-'+id+'">'+
                '<input name="check'+id+'" id="check'+id+'" type="checkbox">'

            HTML += clinsOeil[i].name
            HTML += ' </label> <span> '+ clinsOeil[i].points + ' point' + s + '</span></div>'
        }

        HTML += '</div>';

        return HTML;

    }
    loadHtml(){
        let elComp = document.getElementById('arbreComp');
        let elClinOeil = document.getElementById('contentClinOeil');
        let elMission = document.getElementById('containerMissions');
        // il existe un pollyfill pour la methode after !! pas mis encore
        elComp.innerHTML = elComp.innerHTML + this.Html.competences;
        elClinOeil.innerHTML = elClinOeil.innerHTML + this.Html.clinOeil;
        elMission.innerHTML = elMission.innerHTML + this.Html.missions;


    }
    getValeurs(){
        console.log('comp : '+ this.player.pointsCompetences);
        console.log('lvl :' + this.player.level);
        console.log('journee :' + this.journee);
        console.log(this.Html);
    }

}

let partie = new Partie({
    missions : [
        {   name : 'Boulangerie',
            butins : '0',
            victimes : '0',
            points : 8
        },
        { name : 'Bibliothèque',
                    butins : '0',
                    victimes : '0',
                    points : 7
            },
            {   name : 'Boutique Hugo Le Chef',
                butins : '0',
                victimes : '0',
                points : 8
            },
            {   name : 'Ventre et achat d\'or',
                butins : '0',
                victimes : '0',
                points : 5
            },
            {   name : 'Boutique High-Tech',
                butins : '0',
                victimes : '0',
                points : 7
            },
            {   name : 'Banque',
                butins : '0',
                victimes : '0',
                points : 12
            },
            {   name : 'Concessionnaire',
                butins : '0',
                victimes : '0',
                points : 8
            },
            {   name : 'Boutique Jean Jakon',
                butins : '0',
                victimes : '0',
                points : 14
            },
            {   name : 'Casino',
                butins : '0',
                victimes : '0',
                points : 8
            },
            {   name : 'Tiny Heroes Shop',
                butins : '0',
                victimes : '0',
                points : 7
            },
            {   name : 'Mulberry Bar',
                butins : '0',
                victimes : '0',
                points : 6
            },
            {   name : 'ASAP Poker Club',
                butins : '0',
                victimes : '0',
                points : 9
            },
            {   name : 'Salon De Manucure',
                butins : '0',
                victimes : '0',
                points : 1
            },
            {   name : 'Armurerie "La Coulevrine"',
                butins : '0',
                victimes : '0',
                points : 5
            },
            {   name : 'Bijouterie LuLu Aristode',
                butins : '0',
                victimes : '0',
                points : 8
            },
            {   name : 'l\'Albatros',
                butins : '0',
                victimes : '0',
                points : 23
            },
            {   name : 'Cinéma "24 Rêves"',
                butins : '0',
                victimes : '0',
                points : 5
            },
            {   name : 'Bookmarker',
                butins : '0',
                victimes : '0',
                points : 'x'
            },
            {   name : 'Librairie Makaka',
                butins : '0',
                victimes : '0',
                points : 'x'
            }
    ],
    clinsOeil : [
        {name: 'Homer Simpson',points: 1},
        {name: 'Heinsenberg (BreakingBad)',points:1},
        {name: 'Jerry (Mystery)',points:2},
        {name: 'L\'Armée des 12 singes',points:2},
        {name: 'Sherlock Holmes (Makaka)',points:3},
        {name: 'Les Nerfs a vifs',points:3},
        {name: '2001 l\'Odyssée de l\'espace',points:3},
        {name: 'Katz (Makaka)',points:2},
        {name: 'La Guilde des voleurs (Makaka)',points:1},
        {name: 'Reservoir Dogs',points:3},
        {name: 'Pulp Fictions',points:3},
        {name: 'Spirou',points:1},
        {name: 'Assassin(s)',points:1},
        {name: 'Saul Goodman',points:1},
        {name:'Your Town (Makaka)' ,points:1},
        {name: 'Monstres et compagnie',points:2},
        {name: 'Définition (Makaka)',points:2},
        {name: 'Better Call Saul',points:2},
        {name: 'La Haine',points:1},
        {name: 'Iron Man',points:2},
        {name: 'Atma (Makaka)',points:3}
    ],
    competences : [
        { id : 'neut' ,name: 'Neutralisateur', nbrNiveaux : 3 },
        { id : 'chau' ,name: 'Chauffeur', nbrNiveaux : 5 },
        { id : 'serr' ,name: 'Serrurier', nbrNiveaux : 5 },
        { id : 'disc' ,name: 'Discrétion', nbrNiveaux : 5 },
        { id : 'tire' ,name: 'Tireur D\'élite', nbrNiveaux : 5 },
        { id : 'repe' ,name: 'Repérage', nbrNiveaux : 4 },
        { id : 'char' ,name: 'Charisme', nbrNiveaux : 4 }
    ]
});
export default partie;




















export default class Competence{
    constructor(datas){
        this.name = datas.name;
        this.nbrNiveaux = datas.nbrNiveaux;
        this.id = datas.id;
    }
    CreateHtml()
    {
        let HTML = '<div id="comp-' + this.id + '" class="competence"> '+
            '<div class="containerCompTitre"><h2>' + this.name + '</h2></div>' +
            '<ul class="grid-5">'

        for (var i=1; i <= this.nbrNiveaux ; i++){
            HTML += '<li>Niveau '+i+'</li>'
        }

        HTML += '</ul></div>';
        return HTML;
    }
}